library reorderable_grid;

import 'dart:ui' show lerpDouble;

import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';
import 'awesome_grid.dart';

class AwesomeGridView extends StatefulWidget {
  const AwesomeGridView.builder({
    Key? key,
    required this.itemBuilder,
    required this.itemCount,
    required this.onReorder,
    required this.gridDelegate,
    this.animationDuration = const Duration(milliseconds: 250),
    this.proxyDecorator,
    this.buildDefaultDragHandles = true,
    this.padding,
    this.header,
    this.scrollDirection = Axis.vertical,
    this.reverse = false,
    this.scrollController,
    this.primary,
    this.physics,
    this.shrinkWrap = false,
    this.anchor = 0.0,
    this.cacheExtent,
    this.dragStartBehavior = DragStartBehavior.start,
    this.keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
    this.restorationId,
    this.clipBehavior = Clip.hardEdge,
  })  : assert(itemCount >= 0),
        super(key: key);

  final AnimatedIndexedWidgetBuilder itemBuilder;

  final int itemCount;

  final ReorderCallback onReorder;

  final SliverGridDelegate gridDelegate;

  final ReorderItemProxyDecorator? proxyDecorator;

  final Duration animationDuration;

  final bool buildDefaultDragHandles;

  final EdgeInsets? padding;

  final Widget? header;

  final Axis scrollDirection;

  final bool reverse;

  final ScrollController? scrollController;

  final bool? primary;

  final ScrollPhysics? physics;

  final bool shrinkWrap;

  final double anchor;

  final double? cacheExtent;

  final DragStartBehavior dragStartBehavior;

  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;

  final String? restorationId;

  final Clip clipBehavior;

  @override
  AwesomeGridViewState createState() => AwesomeGridViewState();
}

class AwesomeGridViewState extends State<AwesomeGridView> {
  late OverlayEntry _listOverlayEntry;

  final _reorderableGridContentKey = GlobalKey<_AwesomeGridContentState>();

  @override
  void initState() {
    super.initState();
    _listOverlayEntry = OverlayEntry(
      opaque: true,
      builder: (BuildContext context) {
        return _AwesomeGridContent(
          key: _reorderableGridContentKey,
          itemBuilder: widget.itemBuilder,
          itemCount: widget.itemCount,
          onReorder: widget.onReorder,
          gridDelegate: widget.gridDelegate,
          proxyDecorator: widget.proxyDecorator,
          animationDuration: widget.animationDuration,
          buildDefaultDragHandles: widget.buildDefaultDragHandles,
          padding: widget.padding,
          header: widget.header,
          scrollDirection: widget.scrollDirection,
          reverse: widget.reverse,
          scrollController: widget.scrollController,
          primary: widget.primary,
          physics: widget.physics,
          shrinkWrap: widget.shrinkWrap,
          anchor: widget.anchor,
          cacheExtent: widget.cacheExtent,
          dragStartBehavior: widget.dragStartBehavior,
          keyboardDismissBehavior: widget.keyboardDismissBehavior,
          restorationId: widget.restorationId,
          clipBehavior: widget.clipBehavior,
        );
      },
    );
  }

  void insertItem(int index) {
    _reorderableGridContentKey.currentState!.insertItem(index);
  }

  void removeItem(int index) {
    _reorderableGridContentKey.currentState!.removeItem(index);
  }

  void startChanges() {
    return _reorderableGridContentKey.currentState!.startChanges();
  }

  Future<void> commitChanges() {
    return _reorderableGridContentKey.currentState!.commitChanges();
  }

  @override
  void didUpdateWidget(AwesomeGridView oldWidget) {
    super.didUpdateWidget(oldWidget);

    _listOverlayEntry.markNeedsBuild();
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterialLocalizations(context));
    return Overlay(
      initialEntries: <OverlayEntry>[_listOverlayEntry],
    );
  }
}

class _AwesomeGridContent extends StatefulWidget {
  const _AwesomeGridContent({
    required Key key,
    required this.itemBuilder,
    required this.itemCount,
    required this.onReorder,
    required this.gridDelegate,
    required this.proxyDecorator,
    required this.animationDuration,
    required this.buildDefaultDragHandles,
    required this.padding,
    required this.header,
    required this.scrollDirection,
    required this.reverse,
    required this.scrollController,
    required this.primary,
    required this.physics,
    required this.shrinkWrap,
    required this.anchor,
    required this.cacheExtent,
    required this.dragStartBehavior,
    required this.keyboardDismissBehavior,
    required this.restorationId,
    required this.clipBehavior,
  }) : super(key: key);

  final AnimatedIndexedWidgetBuilder itemBuilder;
  final int itemCount;
  final ReorderCallback onReorder;
  final SliverGridDelegate gridDelegate;
  final ReorderItemProxyDecorator? proxyDecorator;
  final Duration animationDuration;
  final bool buildDefaultDragHandles;
  final EdgeInsets? padding;
  final Widget? header;
  final Axis scrollDirection;
  final bool reverse;
  final ScrollController? scrollController;
  final bool? primary;
  final ScrollPhysics? physics;
  final bool shrinkWrap;
  final double anchor;
  final double? cacheExtent;
  final DragStartBehavior dragStartBehavior;
  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;
  final String? restorationId;
  final Clip clipBehavior;

  @override
  _AwesomeGridContentState createState() => _AwesomeGridContentState();
}

class _AwesomeGridContentState extends State<_AwesomeGridContent> {
  final _sliverAwesomeGridKey = GlobalKey<SliverAwesomeGridState>();

  void insertItem(int index) {
    _sliverAwesomeGridKey.currentState!.insertItem(index);
  }

  void removeItem(int index) {
    _sliverAwesomeGridKey.currentState!.removeItem(index);
  }

  void startChanges() {
    return _sliverAwesomeGridKey.currentState!.startChanges();
  }

  Future<void> commitChanges() {
    return _sliverAwesomeGridKey.currentState!.commitChanges();
  }

  Widget _wrapWithSemantics(Widget child, int index) {
    void reorder(int startIndex, int endIndex) {
      if (startIndex != endIndex) widget.onReorder(startIndex, endIndex);
    }

    final Map<CustomSemanticsAction, VoidCallback> semanticsActions =
        <CustomSemanticsAction, VoidCallback>{};

    void moveToStart() => reorder(index, 0);
    void moveToEnd() => reorder(index, widget.itemCount);
    void moveBefore() => reorder(index, index - 1);

    void moveAfter() => reorder(index, index + 2);

    final MaterialLocalizations localizations =
        MaterialLocalizations.of(context);

    if (index > 0) {
      semanticsActions[
              CustomSemanticsAction(label: localizations.reorderItemToStart)] =
          moveToStart;
      String reorderItemBefore = localizations.reorderItemUp;
      if (widget.scrollDirection == Axis.horizontal) {
        reorderItemBefore = Directionality.of(context) == TextDirection.ltr
            ? localizations.reorderItemLeft
            : localizations.reorderItemRight;
      }
      semanticsActions[CustomSemanticsAction(label: reorderItemBefore)] =
          moveBefore;
    }

    if (index < widget.itemCount - 1) {
      String reorderItemAfter = localizations.reorderItemDown;
      if (widget.scrollDirection == Axis.horizontal) {
        reorderItemAfter = Directionality.of(context) == TextDirection.ltr
            ? localizations.reorderItemRight
            : localizations.reorderItemLeft;
      }
      semanticsActions[CustomSemanticsAction(label: reorderItemAfter)] =
          moveAfter;
      semanticsActions[
              CustomSemanticsAction(label: localizations.reorderItemToEnd)] =
          moveToEnd;
    }

    return MergeSemantics(
      child: Semantics(
        customSemanticsActions: semanticsActions,
        child: child,
      ),
    );
  }

  Widget _itemBuilder(BuildContext context, int index, Animation animation) {
    final Widget item = widget.itemBuilder(context, index, animation);

    final Widget itemWithSemantics = _wrapWithSemantics(item, index);
    final Key itemGlobalKey = _AwesomeGridViewChildGlobalKey(item.key!, this);

    if (widget.buildDefaultDragHandles) {
      switch (Theme.of(context).platform) {
        case TargetPlatform.fuchsia:
        case TargetPlatform.linux:
        case TargetPlatform.windows:
        case TargetPlatform.macOS:
          return Stack(
            key: itemGlobalKey,
            children: <Widget>[
              itemWithSemantics,
              Positioned.directional(
                textDirection: Directionality.of(context),
                top: 0,
                bottom: 0,
                end: 8,
                child: Align(
                  alignment: AlignmentDirectional.centerEnd,
                  child: AwesomeGridDragStartListener(
                    index: index,
                    child: const Icon(Icons.drag_handle),
                  ),
                ),
              ),
            ],
          );

        case TargetPlatform.iOS:
        case TargetPlatform.android:
          return AwesomeGridDelayedDragStartListener(
            key: itemGlobalKey,
            index: index,
            child: itemWithSemantics,
          );
      }
    }

    return KeyedSubtree(
      key: itemGlobalKey,
      child: itemWithSemantics,
    );
  }

  Widget _proxyDecorator(Widget child, int index, Animation<double> animation) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        final double animValue = Curves.easeInOut.transform(animation.value);
        final double elevation = lerpDouble(0, 6, animValue)!;
        return Material(
          child: child,
          elevation: elevation,
        );
      },
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final EdgeInsets padding = widget.padding ?? EdgeInsets.zero;

    return CustomScrollView(
      scrollDirection: widget.scrollDirection,
      reverse: widget.reverse,
      controller: widget.scrollController,
      primary: widget.primary,
      physics: widget.physics,
      shrinkWrap: widget.shrinkWrap,
      anchor: widget.anchor,
      cacheExtent: widget.cacheExtent,
      dragStartBehavior: widget.dragStartBehavior,
      keyboardDismissBehavior: widget.keyboardDismissBehavior,
      restorationId: widget.restorationId,
      clipBehavior: widget.clipBehavior,
      slivers: <Widget>[
        if (widget.header != null) SliverToBoxAdapter(child: widget.header!),
        SliverPadding(
          padding: padding,
          sliver: SliverAwesomeGrid(
            key: _sliverAwesomeGridKey,
            itemBuilder: _itemBuilder,
            itemCount: widget.itemCount,
            animationDuration: widget.animationDuration,
            onReorder: widget.onReorder,
            gridDelegate: widget.gridDelegate,
            proxyDecorator: widget.proxyDecorator ?? _proxyDecorator,
          ),
        ),
      ],
    );
  }
}

@optionalTypeArgs
class _AwesomeGridViewChildGlobalKey extends GlobalObjectKey {
  const _AwesomeGridViewChildGlobalKey(this.subKey, this.state) : super(subKey);

  final Key subKey;
  final State state;

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) return false;
    return other is _AwesomeGridViewChildGlobalKey &&
        other.subKey == subKey &&
        other.state == state;
  }

  @override
  int get hashCode => hashValues(subKey, state);
}
