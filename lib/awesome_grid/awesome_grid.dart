// Copyright 2014 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';

typedef AwesomeGridCallback = void Function(int oldIndex, int newIndex);

typedef AwesomeGridItemProxyDecorator = Widget Function(
    Widget child, int index, Animation<double> animation);

typedef AnimatedIndexedWidgetBuilder = Widget Function(
    BuildContext context, int index, Animation animation);

class AwesomeGrid extends StatefulWidget {
  const AwesomeGrid({
    Key? key,
    required this.itemBuilder,
    required this.itemCount,
    required this.onReorder,
    required this.gridDelegate,
    this.proxyDecorator,
    this.animationDuration = const Duration(milliseconds: 250),
    this.padding,
    this.scrollDirection = Axis.vertical,
    this.reverse = false,
    this.controller,
    this.primary,
    this.physics,
    this.shrinkWrap = false,
    this.anchor = 0.0,
    this.cacheExtent,
    this.dragStartBehavior = DragStartBehavior.start,
    this.keyboardDismissBehavior = ScrollViewKeyboardDismissBehavior.manual,
    this.restorationId,
    this.clipBehavior = Clip.hardEdge,
  })  : assert(itemCount >= 0),
        super(key: key);

  final AnimatedIndexedWidgetBuilder itemBuilder;

  final int itemCount;

  final AwesomeGridCallback onReorder;

  final SliverGridDelegate gridDelegate;

  final AwesomeGridItemProxyDecorator? proxyDecorator;

  final Duration animationDuration;

  final EdgeInsetsGeometry? padding;

  final Axis scrollDirection;

  final bool reverse;

  final ScrollController? controller;

  final bool? primary;

  final ScrollPhysics? physics;

  final bool shrinkWrap;

  final double anchor;

  final double? cacheExtent;

  final DragStartBehavior dragStartBehavior;

  final ScrollViewKeyboardDismissBehavior keyboardDismissBehavior;

  final String? restorationId;

  final Clip clipBehavior;

  static AwesomeGridState of(BuildContext context) {
    final AwesomeGridState? result =
        context.findAncestorStateOfType<AwesomeGridState>();
    assert(() {
      if (result == null) {
        throw FlutterError.fromParts(<DiagnosticsNode>[
          ErrorSummary(
              'AwesomeGrid.of() called with a context that does not contain a AwesomeGrid.'),
          ErrorDescription(
              'No AwesomeGrid ancestor could be found starting from the context that was passed to AwesomeGrid.of().'),
          ErrorHint(
              'This can happen when the context provided is from the same StatefulWidget that '
              'built the AwesomeGrid. Please see the AwesomeGrid documentation for examples '
              'of how to refer to an AwesomeGridState object:'
              '  https://api.flutter.dev/flutter/widgets/AwesomeGridState-class.html'),
          context.describeElement('The context used was')
        ]);
      }
      return true;
    }());
    return result!;
  }

  static AwesomeGridState? maybeOf(BuildContext context) {
    return context.findAncestorStateOfType<AwesomeGridState>();
  }

  @override
  AwesomeGridState createState() => AwesomeGridState();
}

class AwesomeGridState extends State<AwesomeGrid> {
  final GlobalKey<SliverAwesomeGridState> _sliverAwesomeGridKey = GlobalKey();

  void startItemDragReorder({
    required int index,
    required PointerDownEvent event,
    required MultiDragGestureRecognizer<MultiDragPointerState> recognizer,
  }) {
    _sliverAwesomeGridKey.currentState!.startItemDragReorder(
        index: index, event: event, recognizer: recognizer);
  }

  void cancelReorder() {
    _sliverAwesomeGridKey.currentState!.cancelReorder();
  }

  void insertItem(int index) {
    _sliverAwesomeGridKey.currentState!.insertItem(index);
  }

  void removeItem(int index) {
    _sliverAwesomeGridKey.currentState!.removeItem(index);
  }

  void startChanges() {
    _sliverAwesomeGridKey.currentState!.startChanges();
  }

  void commitChanges() {
    _sliverAwesomeGridKey.currentState!.commitChanges();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      scrollDirection: widget.scrollDirection,
      reverse: widget.reverse,
      controller: widget.controller,
      primary: widget.primary,
      physics: widget.physics,
      shrinkWrap: widget.shrinkWrap,
      anchor: widget.anchor,
      cacheExtent: widget.cacheExtent,
      dragStartBehavior: widget.dragStartBehavior,
      keyboardDismissBehavior: widget.keyboardDismissBehavior,
      restorationId: widget.restorationId,
      clipBehavior: widget.clipBehavior,
      slivers: <Widget>[
        SliverPadding(
          padding: widget.padding ?? EdgeInsets.zero,
          sliver: SliverAwesomeGrid(
            key: _sliverAwesomeGridKey,
            itemBuilder: widget.itemBuilder,
            itemCount: widget.itemCount,
            animationDuration: widget.animationDuration,
            onReorder: widget.onReorder,
            gridDelegate: widget.gridDelegate,
            proxyDecorator: widget.proxyDecorator,
          ),
        ),
      ],
    );
  }
}

class SliverAwesomeGrid extends StatefulWidget {
  const SliverAwesomeGrid({
    Key? key,
    required this.itemBuilder,
    required this.itemCount,
    required this.animationDuration,
    required this.onReorder,
    required this.gridDelegate,
    this.proxyDecorator,
  })  : assert(itemCount >= 0),
        super(key: key);

  final AnimatedIndexedWidgetBuilder itemBuilder;

  final int itemCount;

  final Duration animationDuration;

  final AwesomeGridCallback onReorder;

  final SliverGridDelegate gridDelegate;

  final AwesomeGridItemProxyDecorator? proxyDecorator;

  @override
  SliverAwesomeGridState createState() => SliverAwesomeGridState();

  static SliverAwesomeGridState of(BuildContext context) {
    final SliverAwesomeGridState? result =
        context.findAncestorStateOfType<SliverAwesomeGridState>();
    assert(() {
      if (result == null) {
        throw FlutterError.fromParts(<DiagnosticsNode>[
          ErrorSummary(
              'SliverAwesomeGrid.of() called with a context that does not contain a SliverAwesomeGrid.'),
          ErrorDescription(
              'No SliverAwesomeGrid ancestor could be found starting from the context that was passed to SliverAwesomeGrid.of().'),
          ErrorHint(
              'This can happen when the context provided is from the same StatefulWidget that '
              'built the SliverAwesomeGrid. Please see the SliverAwesomeGrid documentation for examples '
              'of how to refer to an SliverAwesomeGrid object:'
              '  https://api.flutter.dev/flutter/widgets/SliverAwesomeGridState-class.html'),
          context.describeElement('The context used was')
        ]);
      }
      return true;
    }());
    return result!;
  }

  static SliverAwesomeGridState? maybeOf(BuildContext context) {
    return context.findAncestorStateOfType<SliverAwesomeGridState>();
  }
}

class SliverAwesomeGridState extends State<SliverAwesomeGrid>
    with TickerProviderStateMixin {
  // Map of index -> child state used manage where the dragging item will need
  // to be inserted.
  final Map<int, _ReorderableItemState> _items = <int, _ReorderableItemState>{};

  bool _reorderingDrag = false;
  bool _autoScrolling = false;
  OverlayEntry? _overlayEntry;
  _ReorderableItemState? _dragItem;
  _DragInfo? _dragInfo;
  int? _insertIndex;
  Offset? _finalDropPosition;
  MultiDragGestureRecognizer<MultiDragPointerState>? _recognizer;

  late ScrollableState _scrollable;
  Axis get _scrollDirection => axisDirectionToAxis(_scrollable.axisDirection);
  bool get _reverse =>
      _scrollable.axisDirection == AxisDirection.up ||
      _scrollable.axisDirection == AxisDirection.left;

  final List<_TemporaryListItem> _incomingItems = <_TemporaryListItem>[];
  final List<_TemporaryListItem> _outgoingItems = <_TemporaryListItem>[];

  int _itemCount = 0;

  final gridKey = GlobalKey();

  bool _committingChanges = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _scrollable = Scrollable.of(context)!;
  }

  @override
  void didUpdateWidget(covariant SliverAwesomeGrid oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.itemCount != oldWidget.itemCount) {
      cancelReorder();
      setState(() {
        _itemCount = widget.itemCount;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _itemCount = widget.itemCount;
  }

  @override
  void dispose() {
    _dragInfo?.dispose();

    super.dispose();
  }

  void startChanges() {
    final stage = _items.entries
        .where((entry) => entry.value.mounted)
        .map((entry) => entry.value.index)
        .toList();

    for (var index = 0; index < stage.length; index++) {
      if (stage[index] == -1) continue;

      final childItem = _items[stage[index]]!;

      childItem.resetGap();
    }
  }

  Future<void> commitChanges() async {
    // assert(!_committingChanges);

    // _committingChanges = true;

    final stage = _items.entries
        .where((entry) => entry.value.mounted)
        .map((entry) => entry.value.index)
        .toList();

    // Items can be added to _items in reverse order, depending on the
    // direction of the last scroll
    stage.sort();

    final offset = stage.isEmpty ? 0 : stage.first;
    int change = 0;

    _outgoingItems.forEach((item) {
      change -= 1;

      if (item.itemIndex - offset < 0) return;

      stage.removeAt(item.itemIndex - offset);

      _items[item.itemIndex]?.widget.controller.reverse();
    });

    _incomingItems.forEach((item) {
      change += 1;

      if (item.itemIndex - offset < 0) return;

      stage.insert(item.itemIndex - offset, -1);
    });

    List<Future> futures = [];

    for (var index = 0; index < stage.length; index++) {
      if (stage[index] == -1) continue;

      final childItem = _items[stage[index]]!;

      futures.add(
          childItem.shift(index + offset - stage[index], _reverse).then((_) {
        childItem.resetGap();
      }));
    }

    await Future.wait(futures);

    setState(() {
      _outgoingItems.clear();
      _itemCount += change;
    });

    // _committingChanges = false;
  }

  void removeItem(int index) {
    assert(!_committingChanges);

    _outgoingItems
      ..add(_TemporaryListItem.index(index))
      ..sort();
  }

  void insertItem(int index) async {
    assert(!_committingChanges);

    // Account for the insertion
    for (final _TemporaryListItem item in _incomingItems) {
      if (item.itemIndex >= index) item.itemIndex += 1;
    }

    _incomingItems
      ..add(_TemporaryListItem.index(index))
      ..sort();
  }

  void startItemDragReorder({
    required int index,
    required PointerDownEvent event,
    required MultiDragGestureRecognizer<MultiDragPointerState> recognizer,
  }) {
    assert(0 <= index && index < _itemCount);
    setState(() {
      if (_reorderingDrag) {
        cancelReorder();
      }
      if (_items.containsKey(index)) {
        _dragItem = _items[index]!;
        _recognizer = recognizer
          ..onStart = _dragStart
          ..addPointer(event);
      } else {
        throw Exception('Attempting to start a drag on a non-visible item');
      }
    });
  }

  void cancelReorder() {
    _dragReset();
  }

  void _registerItem(_ReorderableItemState item) {
    _items[item.index] = item;
  }

  void _unregisterItem(int index, _ReorderableItemState item) {
    final _ReorderableItemState? currentItem = _items[index];
    if (currentItem == item) {
      _items.remove(index);
    }
  }

  Drag? _dragStart(Offset position) {
    assert(_reorderingDrag == false);
    final _ReorderableItemState item = _dragItem!;

    _insertIndex = item.index;
    _reorderingDrag = true;
    _dragInfo = _DragInfo(
      item: item,
      initialPosition: position,
      scrollDirection: _scrollDirection,
      onUpdate: _dragUpdate,
      onCancel: _dragCancel,
      onEnd: _dragEnd,
      onDropCompleted: _dropCompleted,
      proxyDecorator: widget.proxyDecorator,
      tickerProvider: this,
      animationDuration: widget.animationDuration,
    );

    final OverlayState overlay = Overlay.of(context)!;
    assert(_overlayEntry == null);
    _overlayEntry = OverlayEntry(builder: _dragInfo!.createProxy);
    overlay.insert(_overlayEntry!);

    _dragInfo!.startDrag();

    item.dragging = true;
    for (final _ReorderableItemState childItem in _items.values) {
      if (childItem == item || !childItem.mounted) continue;
      childItem.updateForGap(_insertIndex!, _dragInfo!.item, false, _reverse);
    }
    return _dragInfo;
  }

  void _dragUpdate(_DragInfo item, Offset position, Offset delta) {
    setState(() {
      _overlayEntry?.markNeedsBuild();
      _dragUpdateItems();
      _autoScrollIfNecessary();
    });
  }

  void _dragCancel(_DragInfo item) {
    _dragReset();
  }

  void _dragEnd(_DragInfo item) {
    setState(() {
      _finalDropPosition = _itemOffsetAt(_insertIndex!);
    });
  }

  void _dropCompleted() {
    final int fromIndex = _dragItem!.index;
    final int toIndex = _insertIndex!;
    if (fromIndex != toIndex) {
      widget.onReorder.call(fromIndex, toIndex);
    }
    _dragReset();
  }

  void _dragReset() {
    setState(() {
      if (_reorderingDrag) {
        _reorderingDrag = false;
        _dragItem!.dragging = false;
        _dragItem = null;
        _dragInfo?.dispose();
        _dragInfo = null;
        _resetItemGap();
        _recognizer?.dispose();
        _recognizer = null;
        _overlayEntry?.remove();
        _overlayEntry = null;
        _finalDropPosition = null;
      }
    });
  }

  void _resetItemGap() {
    for (final _ReorderableItemState item in _items.values) {
      item.resetGap();
    }
  }

  void _dragUpdateItems() {
    assert(_reorderingDrag);
    assert(_dragItem != null);
    assert(_dragInfo != null);
    final _ReorderableItemState gapItem = _dragItem!;
    final Size gapSize = _dragInfo!.itemSize;

    // Find the new index for inserting the item being dragged.
    int newIndex = _insertIndex!;

    final topLeft = _dragInfo!.dragPosition - _dragInfo!.dragOffset;
    final proxyGeometry =
        Rect.fromLTWH(topLeft.dx, topLeft.dy, gapSize.width, gapSize.height);

    for (final _ReorderableItemState item in _items.values) {
      if (!item.mounted) continue;

      // Correct the transform to calculate with the actual position
      final Rect itemGeometry =
          item.targetGeometry().translate(-item.offset.dx, -item.offset.dy);

      if (!proxyGeometry.overlaps(itemGeometry)) {
        continue;
      }

      final intersection = itemGeometry.intersect(proxyGeometry);

      if (intersection.size > gapSize / 2) {
        newIndex = item.index;
        break;
      }
    }

    if (newIndex != _insertIndex) {
      _insertIndex = newIndex;
      for (final _ReorderableItemState item in _items.values) {
        if (item == gapItem || !item.mounted) continue;
        item.updateForGap(newIndex, gapItem, true, _reverse);
      }
    }
  }

  Future<void> _autoScrollIfNecessary() async {
    if (!_autoScrolling && _dragInfo != null && _dragInfo!.scrollable != null) {
      final ScrollPosition position = _dragInfo!.scrollable!.position;
      double? newOffset;
      const Duration duration = Duration(milliseconds: 14);
      const double step = 1.0;
      const double overDragMax = 20.0;
      const double overDragCoef = 5;

      final RenderBox scrollRenderBox =
          _dragInfo!.scrollable!.context.findRenderObject()! as RenderBox;
      final Offset scrollOrigin = scrollRenderBox.localToGlobal(Offset.zero);
      final double scrollStart = _offsetExtent(scrollOrigin, _scrollDirection);
      final double scrollEnd =
          scrollStart + _sizeExtent(scrollRenderBox.size, _scrollDirection);

      final double proxyStart = _offsetExtent(
          _dragInfo!.dragPosition - _dragInfo!.dragOffset, _scrollDirection);
      final double proxyEnd = proxyStart + _dragInfo!.itemExtent;

      if (_reverse) {
        if (proxyEnd > scrollEnd &&
            position.pixels > position.minScrollExtent) {
          final double overDrag = max(proxyEnd - scrollEnd, overDragMax);
          newOffset = max(position.minScrollExtent,
              position.pixels - step * overDrag / overDragCoef);
        } else if (proxyStart < scrollStart &&
            position.pixels < position.maxScrollExtent) {
          final double overDrag = max(scrollStart - proxyStart, overDragMax);
          newOffset = min(position.maxScrollExtent,
              position.pixels + step * overDrag / overDragCoef);
        }
      } else {
        if (proxyStart < scrollStart &&
            position.pixels > position.minScrollExtent) {
          final double overDrag = max(scrollStart - proxyStart, overDragMax);
          newOffset = max(position.minScrollExtent,
              position.pixels - step * overDrag / overDragCoef);
        } else if (proxyEnd > scrollEnd &&
            position.pixels < position.maxScrollExtent) {
          final double overDrag = max(proxyEnd - scrollEnd, overDragMax);
          newOffset = min(position.maxScrollExtent,
              position.pixels + step * overDrag / overDragCoef);
        }
      }

      if (newOffset != null && (newOffset - position.pixels).abs() >= 1.0) {
        _autoScrolling = true;
        await position.animateTo(newOffset,
            duration: duration, curve: Curves.linear);
        _autoScrolling = false;
        if (_dragItem != null) {
          _dragUpdateItems();
          _autoScrollIfNecessary();
        }
      }
    }
  }

  Offset _itemOffsetAt(int index) {
    final RenderBox itemRenderBox =
        _items[index]!.context.findRenderObject()! as RenderBox;
    return itemRenderBox.localToGlobal(Offset.zero);
  }

  _TemporaryListItem? _removeActiveItemAt(
      List<_TemporaryListItem> items, int itemIndex) {
    final int i = binarySearch(items, _TemporaryListItem.index(itemIndex));
    return i == -1 ? null : items.removeAt(i);
  }

  _TemporaryListItem? _activeItemAt(
      List<_TemporaryListItem> items, int itemIndex) {
    final int i = binarySearch(items, _TemporaryListItem.index(itemIndex));
    return i == -1 ? null : items[i];
  }

  Widget _itemBuilder(BuildContext context, int index) {
    final incomingItem = _activeItemAt(_incomingItems, index);

    final controller = AnimationController(
      vsync: this,
      duration: widget.animationDuration,
    );

    if (incomingItem != null) {
      controller.forward().then((_) {
        _removeActiveItemAt(_incomingItems, incomingItem.itemIndex);
      });
    } else {
      // Mark this animation as finished since this item is not being added now
      controller.value = 1.0;
    }

    final child = widget.itemBuilder(context, index, controller.view);

    assert(child.key != null, 'All list items must have a key');
    final OverlayState overlay = Overlay.of(context)!;
    return _ReorderableItem(
      key: _ReorderableItemGlobalKey(child.key!, index, this),
      index: index,
      // child: child,
      builder: widget.itemBuilder,
      controller: controller,
      animationDuration: widget.animationDuration,
      capturedThemes:
          InheritedTheme.capture(from: context, to: overlay.context),
    );
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasOverlay(context));
    return SliverGrid(
      key: gridKey,
      gridDelegate: widget.gridDelegate,
      delegate: SliverChildBuilderDelegate(
        _itemBuilder,
        childCount: _itemCount,
      ),
    );
  }
}

class _ReorderableItem extends StatefulWidget {
  const _ReorderableItem({
    required Key key,
    required this.index,
    // required this.child,
    required this.builder,
    required this.capturedThemes,
    required this.controller,
    required this.animationDuration,
  }) : super(key: key);

  final int index;
  // final Widget child;
  final AnimatedIndexedWidgetBuilder builder;
  final CapturedThemes capturedThemes;
  final AnimationController controller;
  final Duration animationDuration;

  Widget child(BuildContext context) {
    return builder(context, index, controller.view);
  }

  @override
  _ReorderableItemState createState() => _ReorderableItemState();
}

class _ReorderableItemState extends State<_ReorderableItem> {
  late SliverAwesomeGridState _listState;

  Offset _startOffset = Offset.zero;
  Offset _targetOffset = Offset.zero;
  AnimationController? _offsetAnimation;

  Key get key => widget.key!;
  int get index => widget.index;

  bool get dragging => _dragging;
  set dragging(bool dragging) {
    if (mounted) {
      setState(() {
        _dragging = dragging;
      });
    }
  }

  bool _dragging = false;

  @override
  void initState() {
    _listState = SliverAwesomeGrid.of(context);
    _listState._registerItem(this);
    super.initState();
  }

  @override
  void dispose() {
    _offsetAnimation?.dispose();
    widget.controller.dispose(); // Shouldn't this go in the superclass?
    _listState._unregisterItem(index, this);
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant _ReorderableItem oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.index != widget.index) {
      _listState._unregisterItem(oldWidget.index, this);
      _listState._registerItem(this);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_dragging) {
      return const SizedBox();
    }
    _listState._registerItem(this);

    final child = widget.child(context);

    return Transform(
      transform: Matrix4.translationValues(offset.dx, offset.dy, 0.0),
      child: child,
    );
  }

  @override
  void deactivate() {
    _listState._unregisterItem(index, this);
    super.deactivate();
  }

  Offset get offset {
    if (_offsetAnimation != null) {
      final double animValue =
          Curves.easeInOut.transform(_offsetAnimation!.value);
      return Offset.lerp(_startOffset, _targetOffset, animValue)!;
    }
    return _targetOffset;
  }

  Future<void> shift(int amount, bool reverse) {
    final moveToIndex = index + amount;

    return _animateGap(moveToIndex, true, reverse);
  }

  Future<void> updateForGap(int gapIndex, _ReorderableItemState reorderingItem,
      bool animate, bool reverse) {
    int moveToIndex = index;

    if (!((index < reorderingItem.index && index < gapIndex) ||
        (index > reorderingItem.index && index > gapIndex))) {
      moveToIndex = gapIndex >= reorderingItem.index ? index - 1 : index + 1;
    }

    return _animateGap(moveToIndex, animate, reverse);
  }

  Future<void> _animateGap(int moveToIndex, bool animate, bool reverse) {
    final renderSliver =
        _listState.gridKey.currentContext!.findRenderObject()! as RenderSliver;
    final SliverConstraints constraints = renderSliver.constraints;

    final layout = _listState.widget.gridDelegate.getLayout(constraints);

    final currentGeometry = layout.getGeometryForChildIndex(index);
    final targetGeometry = layout.getGeometryForChildIndex(moveToIndex);

    final targetOffset =
        Offset(targetGeometry.crossAxisOffset, targetGeometry.scrollOffset);

    final currentOffset =
        Offset(currentGeometry.crossAxisOffset, currentGeometry.scrollOffset);

    Offset newTargetOffset = targetOffset - currentOffset;

    if (_listState._scrollDirection == Axis.horizontal) {
      newTargetOffset = Offset(newTargetOffset.dy, newTargetOffset.dx);
    }

    if (reverse) {
      newTargetOffset = _listState._scrollDirection == Axis.vertical
          ? newTargetOffset.scale(1, -1)
          : newTargetOffset.scale(-1, 1);
    }

    Future future = Future.value(null);

    if (newTargetOffset != _targetOffset) {
      _targetOffset = newTargetOffset;
      if (animate) {
        if (_offsetAnimation == null) {
          _offsetAnimation = AnimationController(
            vsync: _listState,
            duration: widget.animationDuration,
          )
            ..addListener(rebuild)
            ..addStatusListener((AnimationStatus status) {
              if (status == AnimationStatus.completed) {
                _startOffset = _targetOffset;
                _offsetAnimation!.dispose();
                _offsetAnimation = null;
              }
            });

          future = _offsetAnimation!.forward();
        } else {
          _startOffset = offset;
          future = _offsetAnimation!.forward(from: 0.0);
        }
      } else {
        if (_offsetAnimation != null) {
          _offsetAnimation!.dispose();
          _offsetAnimation = null;
        }
        _startOffset = _targetOffset;
      }
      rebuild();
    }

    return future;
  }

  void resetGap() {
    if (_offsetAnimation != null) {
      _offsetAnimation!.dispose();
      _offsetAnimation = null;
    }
    _startOffset = Offset.zero;
    _targetOffset = Offset.zero;
    rebuild();
  }

  Rect targetGeometry() {
    final RenderBox itemRenderBox = context.findRenderObject()! as RenderBox;
    final Offset itemPosition =
        itemRenderBox.localToGlobal(Offset.zero) + _targetOffset;
    return itemPosition & itemRenderBox.size;
  }

  void rebuild() {
    if (mounted) {
      setState(() {});
    }
  }
}

class AwesomeGridDragStartListener extends StatelessWidget {
  const AwesomeGridDragStartListener({
    Key? key,
    required this.child,
    required this.index,
  }) : super(key: key);

  final Widget child;

  final int index;

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (PointerDownEvent event) => _startDragging(context, event),
      child: child,
    );
  }

  @protected
  MultiDragGestureRecognizer<MultiDragPointerState> createRecognizer() {
    return ImmediateMultiDragGestureRecognizer(debugOwner: this);
  }

  void _startDragging(BuildContext context, PointerDownEvent event) {
    final SliverAwesomeGridState? list = SliverAwesomeGrid.maybeOf(context);
    list?.startItemDragReorder(
        index: index, event: event, recognizer: createRecognizer());
  }
}

class AwesomeGridDelayedDragStartListener extends AwesomeGridDragStartListener {
  const AwesomeGridDelayedDragStartListener({
    Key? key,
    required Widget child,
    required int index,
  }) : super(key: key, child: child, index: index);

  @override
  MultiDragGestureRecognizer<MultiDragPointerState> createRecognizer() {
    return DelayedMultiDragGestureRecognizer(debugOwner: this);
  }
}

typedef _DragItemUpdate = void Function(
    _DragInfo item, Offset position, Offset delta);
typedef _DragItemCallback = void Function(_DragInfo item);

class _DragInfo extends Drag {
  _DragInfo({
    required this.item,
    Offset initialPosition = Offset.zero,
    this.scrollDirection = Axis.vertical,
    this.onUpdate,
    this.onEnd,
    this.onCancel,
    this.onDropCompleted,
    this.proxyDecorator,
    required this.animationDuration,
    required this.tickerProvider,
  }) {
    final RenderBox itemRenderBox =
        item.context.findRenderObject()! as RenderBox;
    dragPosition = initialPosition;
    dragOffset = itemRenderBox.globalToLocal(initialPosition);
    itemSize = item.context.size!;
    itemExtent = _sizeExtent(itemSize, scrollDirection);
    scrollable = Scrollable.of(item.context);
  }

  final _ReorderableItemState item;
  final Axis scrollDirection;
  final _DragItemUpdate? onUpdate;
  final _DragItemCallback? onEnd;
  final _DragItemCallback? onCancel;
  final VoidCallback? onDropCompleted;
  final AwesomeGridItemProxyDecorator? proxyDecorator;
  final Duration animationDuration;
  final TickerProvider tickerProvider;

  late Offset dragPosition;
  late Offset dragOffset;
  late Size itemSize;
  late double itemExtent;
  ScrollableState? scrollable;
  AnimationController? _proxyAnimation;

  void dispose() {
    _proxyAnimation?.dispose();
  }

  void startDrag() {
    _proxyAnimation = AnimationController(
      vsync: tickerProvider,
      duration: animationDuration,
    )
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.dismissed) {
          _dropCompleted();
        }
      })
      ..forward();
  }

  @override
  void update(DragUpdateDetails details) {
    // final Offset delta = _restrictAxis(details.delta, scrollDirection);
    dragPosition += details.delta;
    onUpdate?.call(this, dragPosition, details.delta);
  }

  @override
  void end(DragEndDetails details) {
    _proxyAnimation!.reverse();
    onEnd?.call(this);
  }

  @override
  void cancel() {
    _proxyAnimation?.dispose();
    _proxyAnimation = null;
    onCancel?.call(this);
  }

  void _dropCompleted() {
    _proxyAnimation?.dispose();
    _proxyAnimation = null;
    onDropCompleted?.call();
  }

  Widget createProxy(BuildContext context) {
    return item.widget.capturedThemes.wrap(_DragItemProxy(
      item: item,
      size: itemSize,
      animation: _proxyAnimation!,
      position: dragPosition - dragOffset - _overlayOrigin(context),
      proxyDecorator: proxyDecorator,
    ));
  }
}

Offset _overlayOrigin(BuildContext context) {
  final OverlayState overlay = Overlay.of(context)!;
  final RenderBox overlayBox = overlay.context.findRenderObject()! as RenderBox;
  return overlayBox.localToGlobal(Offset.zero);
}

class _DragItemProxy extends StatelessWidget {
  const _DragItemProxy({
    Key? key,
    required this.item,
    required this.position,
    required this.size,
    required this.animation,
    required this.proxyDecorator,
  }) : super(key: key);

  final _ReorderableItemState item;
  final Offset position;
  final Size size;
  final AnimationController animation;
  final AwesomeGridItemProxyDecorator? proxyDecorator;

  @override
  Widget build(BuildContext context) {
    final Widget child = item.widget.child(context);
    final Widget proxyChild =
        proxyDecorator?.call(child, item.index, animation.view) ?? child;
    final Offset overlayOrigin = _overlayOrigin(context);

    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        Offset effectivePosition = position;
        final Offset? dropPosition = item._listState._finalDropPosition;
        if (dropPosition != null) {
          effectivePosition = Offset.lerp(dropPosition - overlayOrigin,
              effectivePosition, Curves.easeOut.transform(animation.value))!;
        }

        return Positioned(
          child: SizedBox(
            width: size.width,
            height: size.height,
            child: child,
          ),
          left: effectivePosition.dx,
          top: effectivePosition.dy,
        );
      },
      child: proxyChild,
    );
  }
}

double _sizeExtent(Size size, Axis scrollDirection) {
  switch (scrollDirection) {
    case Axis.horizontal:
      return size.width;
    case Axis.vertical:
      return size.height;
  }
}

double _offsetExtent(Offset offset, Axis scrollDirection) {
  switch (scrollDirection) {
    case Axis.horizontal:
      return offset.dx;
    case Axis.vertical:
      return offset.dy;
  }
}

Offset _extentOffset(double extent, Axis scrollDirection) {
  switch (scrollDirection) {
    case Axis.horizontal:
      return Offset(extent, 0.0);
    case Axis.vertical:
      return Offset(0.0, extent);
  }
}

// A global key that takes its identity from the object and uses a value of a
// particular type to identify itself.
//
// The difference with GlobalObjectKey is that it uses [==] instead of [identical]
// of the objects used to generate widgets.
@optionalTypeArgs
class _ReorderableItemGlobalKey extends GlobalObjectKey {
  const _ReorderableItemGlobalKey(this.subKey, this.index, this.state)
      : super(subKey);

  final Key subKey;
  final int index;
  final SliverAwesomeGridState state;

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) return false;
    return other is _ReorderableItemGlobalKey &&
        other.subKey == subKey &&
        other.index == index &&
        other.state == state;
  }

  @override
  int get hashCode => hashValues(subKey, index, state);
}

class _TemporaryListItem implements Comparable<_TemporaryListItem> {
  _TemporaryListItem.index(this.itemIndex);

  int itemIndex;

  @override
  int compareTo(_TemporaryListItem other) => itemIndex - other.itemIndex;
}
